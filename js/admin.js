(function ($, Drupal) {
	Drupal.behaviors.paragraphsAdmin = {
		attach: function (context, settings) {
			$('.js-paragraph-category').on('click', function(event) {
				event.preventDefault();
				$('.paragraph-wrapper').addClass('paragraph-wrapper--hide');
				$('.paragraph-wrapper.' + $(this).data('toggle')).removeClass('paragraph-wrapper--hide');
			});

			$('.paragraph--has-padding-bottom').each(function() {

				// Hide all but the first slider image for correct height calculation
				$(".paragraph-image-text__slider-image:not(:first)").each(function () {
					$(this).hide();
				});

				// Add original height and padding values as attributes to be used on resizable stop event.
				var originalHeight = parseInt($(this).outerHeight());
				var originalPadding = parseInt($(this).css('padding-bottom'));

				// SHow them again since the height is now set
				$(".paragraph-image-text__slider-image:not(:first)").each(function () {
					$(this).show();
				});

				$(this).attr('data-original-height', originalHeight);
				$(this).attr('data-original-padding', originalPadding);
				$(this).attr('data-min-height', originalHeight - originalPadding);
				$(this).attr('data-max-height', originalHeight - originalPadding + 150);
				$(this).attr('data-padding', originalPadding);

				// Add min and max height attribute so the container can't be resized out of bounds.
				$(this).css('min-height', originalHeight - originalPadding);
				$(this).css('max-height', originalHeight - originalPadding + 150);


				$(this).resizable({
					grid: 10,
					handles: 's',
					resize: function(event, ui) {
						// Manual correction to include padding: set height to include padding and remove bottom padding while dragging (it's complicated! D-:)
						// We add the top and bottom padding to the height
						ui.size.height += parseInt($(ui.element).outerHeight()) - parseInt($(ui.element).height());
						// Then add or remove the difference between the initial padding and the current padding
						// This makes it so that the bottom padding is converted to height while dragging
						// Otherwise the sum of top and bottom padding may be higher than the actual height
						ui.size.height += ($(ui.element).attr('data-original-padding') - parseInt($(ui.element).css('padding-bottom')));

						// Pointer events none on next paragraph to avoid contextual links from showing
						$(ui.element).siblings('.paragraph').css('pointer-events', 'none');

						// Remove bottom padding because we're including it as height
						var padding = Math.min(Math.max(ui.size.height - $(ui.element).data('min-height'), 0), 150);
						// Make sure padding is a multiple of 10
						padding = Math.round(padding / 10) * 10;

						$(ui.element).removeClass('paragraph--padding-bottom-' + $(ui.element).attr('data-original-padding'));

						// Add padding as data attribute to show it in the frontend
						if($(ui.element).attr('data-padding') != padding) {
							$(ui.element).attr('data-padding', padding);
							$(ui.element).find('.ui-resizable-handle').attr('data-padding', padding);
						}

					},
					stop: function(event, ui) {
						// When dragging finishes, we change the new height to padding bottom.
						var padding = parseInt($(ui.element).outerHeight()) - $(ui.element).data('original-height') + $(ui.element).data('original-padding');
						$(ui.element).addClass('paragraph--padding-bottom-' + padding);

						// Collect required information and pass the AJAX call
						var paragraph = $(ui.element).attr('id').substring(13);
						var rootParentType = drupalSettings.vb_paragraphs.root_parent_type;
						var rootParent = drupalSettings.vb_paragraphs.root_parent;

						$(ui.element).addClass('loading');

						Drupal.ajax({
							url: Drupal.url('paragraphs/' + rootParentType + '/' + rootParent + '/paragraphs/' + paragraph + '/padding-bottom/' + padding)
						}).execute();
					}
				});
			});
		}
	};
})(jQuery, Drupal);

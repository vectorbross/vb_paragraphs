(function ($, Drupal) {
	Drupal.behaviors.gallery = {
		attach: function (context, settings) {
			$('.paragraph-gallery__images--grid .colorbox').once('colorbox').each(function() {
				$(this).colorbox({
			        maxWidth: function() {
			          if($(window).width() > 900) {
			            return $(window).width() - 160;
			          } else {
			            return $(window).width() - 80;
			          }
			        },
			        maxHeight: function() {
			          return $(window).height() - 100;
			        }
				});
			})
			$('.paragraph-gallery__images--slider').not('.slick-initialized').slick({
				fade: true,
				adaptiveHeight: true
			});
		}
	};
})(jQuery, Drupal);
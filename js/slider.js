(function ($, Drupal) {
	Drupal.behaviors.slider = {
		attach: function (context, settings) {
			$('.paragraph-image-text__slider').not('.slick-initialized').slick({
				fade: true,
				adaptiveHeight: true
			});
		}
	};
})(jQuery, Drupal);
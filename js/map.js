(function ($, Drupal) {

	var map = [];
	var marker = [];

	/**
	 * Helper function to parse JSON from data attribute.
	 */
	function urldecode(url) {
		return decodeURIComponent(url.replace(/\+/g, ' '));
	}

	/**
	 * Check if there is cookie consent before loading the script or iframe.
	 */
	function mapConsent (settings) {
		if (typeof(Cookiebot) !== "undefined" && Cookiebot.consent.statistics) {
			initIframeMap();
			initJsMap();
		}
	}


	/**
	 * Initializes the iframe map.
	 */
	function initIframeMap() {
		$('.paragraph-map__map--iframe').once('map').each(function() {
			$(this).children('iframe').attr('src', $(this).children('iframe').data('src'));
			$(this).children('.no-cookies').remove();
		});
	}


	/**
	 * Initializes the JS map.
	 */
	function initJsMap() {
		if(typeof(google) === 'undefined' && !$('body').hasClass('gmap-loading') && !$('body').hasClass('gmap-loaded')) {
			$('body').addClass('gmap-loading');
			var script = document.createElement('script');
			script.onload = function () {
				$('body').removeClass('gmap-loading').addClass('gmap-loaded');
				renderJsMap();
			};
			script.src = 'https://maps.googleapis.com/maps/api/js?key=' + $('.paragraph-map__map--js').data('key');
			document.head.appendChild(script);
		} else {
			renderJsMap();
		}
	}


	/**
	 * Render the JS map.
	 */
	function renderJsMap(element) {
		$('.paragraph-map__map--js').once('map').each(function(index) {
			var element = $(this);
			element.removeClass('paragraph-map__map--no-cookies').children('.no-cookies').remove();
			if(element.data('address') != '') {
				element.parent().addClass('paragraph-map__map--has-map');
				var geocoder = new google.maps.Geocoder();
				geocoder.geocode( { 'address': element.data('address') }, function(results, status) {
					if (status == 'OK') {
						var location = { lat: results[0].geometry.location.lat(), lng: results[0].geometry.location.lng() };
						map[index] = new google.maps.Map(element[0], {
							center: location,
							zoom: element.data('zoom'),
							styles: element.data('style').length > 0 ? JSON.parse(urldecode(element.data('style'))) : ''
						});
						marker[index] = new google.maps.Marker({
							map: map[index],
							position: location,
							icon: {
								path: "M14.5,12.4c-2.1,0-3.8-1.7-3.8-3.9c0-2.1,1.7-3.9,3.8-3.9s3.8,1.7,3.8,3.9C18.3,10.7,16.6,12.4,14.5,12.4 M14.5,0C9.8,0,6,3.8,6,8.6c0,4.5,3.8,10.2,6.4,13c1,1,2.1,2.5,2.1,2.5s1.2-1.4,2.2-2.5c2.6-2.7,6.3-8.1,6.3-13 C23,3.8,19.2,0,14.5,0z",
								fillColor: element.data('marker'),
								fillOpacity: 1,
								scale: 2,
								strokeWeight: 0,
								anchor: new google.maps.Point(12, 24)
							}
						});
					} else {
						element.before('<div class="paragraph-map__notfound container"><div class="alert alert-danger">Address <em>' + $('.paragraph-map__map--js').data('address') + '</em> not found!</div></div>');
					}
				});
			}
		});
	}

	$(window).on('load CookiebotOnAccept', function(event) {
		mapConsent(drupalSettings);
	});

	Drupal.behaviors.map = {
		attach: function (context, settings) {
			mapConsent(settings);
		}
	};
})(jQuery, Drupal);
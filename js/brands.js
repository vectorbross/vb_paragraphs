(function ($, Drupal) {
	Drupal.behaviors.brands = {
		attach: function (context, settings) {
			$('.paragraph-brands__brands--slider').not('.slick-initialized').slick({
				centerMode: true,
				variableWidth: true,
				infinite: true
			});
		}
	};
})(jQuery, Drupal);
(function ($, Drupal) {
	Drupal.behaviors.quotes = {
		attach: function (context, settings) {
			$('.paragraph-quotes__quotes--slider').not('.slick-initialized').slick({
				fade: true,
				arrows: false,
				dots: true
			});
		}
	};
})(jQuery, Drupal);
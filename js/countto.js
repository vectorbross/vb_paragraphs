(function ($, Drupal) {
    Drupal.behaviors.counter = {
        attach: function (context, settings) {
            $('.counter__count').not('.counter__count--finished').each(function() {
                var counter = $(this);
                counter.on('inview', function () {
                    if(!counter.hasClass('counter__count--finished')) {
                        counter.countTo({
                            formatter: function (value, options) {
                                var split = value.toFixed(options.decimals).split('.');
                                if(split[0].length > 4) {
                                    return split[0].split('').reverse().join('').replace(/([0-9]{3})/g, "$1 ").split('').reverse().join('') + (split.length > 1 ? ',' + split[1] : '');
                                } else {
                                    return value.toFixed(options.decimals).replace('.', ',');
                                }
                            }
                        });
                        counter.addClass('counter__count--finished');
                    }
                });
            });
        }
    };
})(jQuery, Drupal);
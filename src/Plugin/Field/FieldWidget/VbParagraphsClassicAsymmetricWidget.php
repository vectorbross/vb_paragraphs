<?php

namespace Drupal\vb_paragraphs\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\media\Entity\Media;
use Drupal\paragraphs\ParagraphInterface;
use Drupal\paragraphs_asymmetric_translation_widgets\Plugin\Field\FieldWidget\ParagraphsClassicAsymmetricWidget;


/**
 * Plugin implementation of the 'vb_paragraphs_classic_asymmetric' widget.
 *
 * @FieldWidget(
 *   id = "vb_paragraphs_classic_asymmetric",
 *   label = @Translation("VB Paragraphs Classic Asymmetric"),
 *   description = @Translation("A paragraphs inline form widget that supports asymmetric translations and an option to disable while using frontend UI."),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class VbParagraphsClassicAsymmetricWidget extends ParagraphsClassicAsymmetricWidget {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return parent::defaultSettings() + ['disable' => 1];
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['disable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable'),
      '#description' => $this->t('Disable this field-widget for non-admin users on entity edit pages.'),
      '#default_value' => $this->getSetting('disable'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();
    $disabled = $this->widgetDisabled() ? $this->t('Yes') : $this->t('No');
    $summary[] = $this->t('Widget disabled: @disabled', ['@disabled' => $disabled]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function formMultipleElements(FieldItemListInterface $items, array &$form, FormStateInterface $form_state) {
    $elements = parent::formMultipleElements($items, $form, $form_state);

    if (!$this->widgetDisabled()) {
      return $elements;
    }

    // Hide the widget. First we check permissions.
    // We CANNOT remove the widget from the form! Attach CSS.
    if (!\Drupal::currentUser()->hasPermission('access disabled VB paragraphs widget')) {
      $elements['#attached']['library'][] = 'vb_paragraphs/vb_paragraphs_widget';
    }

    return $elements;
  }

  /**
   * Returns the status of the widget. TRUE or FALSE.
   */
  protected function widgetDisabled() {
    $disabled = $this->getSetting('disable');

    if (isset($disabled) && $disabled == '1') {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Clones a paragraph recursively.
   *
   * Also, in case of a translatable paragraph, updates its original language
   * and removes all other translations.
   *
   * @param \Drupal\paragraphs\ParagraphInterface $paragraph
   *   The paragraph entity to clone.
   * @param string $langcode
   *   Language code for all the clone entities created.
   *
   * @return \Drupal\paragraphs\ParagraphInterface
   *   New paragraph object with the data from the original paragraph. Not
   *   saved. All sub-paragraphs are clones as well.
   */
  protected function createDuplicateWithSingleLanguage(ParagraphInterface $paragraph, $langcode) {
    $duplicate = $paragraph->createDuplicate();

    // Clone all sub-paragraphs recursively.
    foreach ($duplicate->getFields(FALSE) as $field) {
      // @todo: should we support field collections as well?
      if ($field->getFieldDefinition()->getType() == 'entity_reference_revisions' && $field->getFieldDefinition()->getTargetEntityTypeId() == 'paragraph') {
        foreach ($field as $item) {
          $item->entity = $this->createDuplicateWithSingleLanguage($item->entity, $langcode);
        }
      }

      // When duplicating paragraphs, Check if we can translate Media-entities.
      if ($field->getFieldDefinition()->getType() == 'entity_reference') {
        $storage = $field->getFieldDefinition()->getSettings();

        if ($storage['target_type'] == 'media') {
          $media_bundles = $storage['handler_settings']['target_bundles'];

          foreach ($media_bundles as $media_bundle) {
            $value = $duplicate->get($field->getName())->getValue();
            if (isset($value[0]['target_id'])) {
              // Load and translate media entity.
              $media = Media::load($value[0]['target_id']);
              if ($media->isTranslatable() && !$media->hasTranslation($langcode)) {
                  $media->addTranslation($langcode, ['name' => $media->label()]);
                  $media->save();

                  // Entity-label and thumbnail is not correctly saved during addTranslation, so we set the name again.
                  // @todo: Check possible solutions.
                  $media_translation = $media->getTranslation($langcode);
                  $media_translation->setName($media->getName());
                  $media_translation->set('thumbnail', $media->get('thumbnail')->getValue());
                  $media_translation->save();
              }
            }
          }
        }
      }
    }

    // Change the original language and remove possible translations.
    if ($duplicate->isTranslatable()) {
      $duplicate->set('langcode', $langcode);
      foreach ($duplicate->getTranslationLanguages(FALSE) as $language) {
        try {
          $duplicate->removeTranslation($language->getId());
        }
        catch (\InvalidArgumentException $e) {
          // Should never happen.
        }
      }
    }

    return $duplicate;
  }

}

<?php

namespace Drupal\vb_paragraphs\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Class RouteSubscriber.
 *
 * Listens to the dynamic route events.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    if ($route = $collection->get('paragraphs_edit.edit_form')) {
      $route->addDefaults(['_entity_form' => 'paragraph.content']);
      $route->addRequirements([
        '_custom_access' => '\Drupal\vb_paragraphs\Controller\VbParagraphsController::accessEditContent',
        '_permission' => 'edit vb_paragraphs'
      ]);
    }
    if ($route = $collection->get('paragraphs_edit.delete_form')) {
      $route->addRequirements([
        '_custom_access' => '\Drupal\vb_paragraphs\Controller\VbParagraphsController::accessLibraryItem',
        '_permission' => 'delete vb_paragraphs'
      ]);
    }
  }

}
<?php

namespace Drupal\vb_paragraphs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form for configuring the Job node type.
 */
class VbParagraphsAdminForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_paragraphs_admin_form';
  }

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('language_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'vb_paragraphs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('vb_paragraphs.settings');

    $form['default_unpublished'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('New paragraphs are unpublished by default.'),
      '#default_value' => $config->get('default_unpublished'),
    ];

    // Settings for paragraph templates.
    $node_bundles = \Drupal::entityTypeManager()
      ->getStorage('node_type')
      ->loadMultiple();

    $form['paragraph_templates'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Define Paragraph template-node for each node-type.')
    ];

    foreach ($node_bundles as $node_bundle) {
      $default_entity = FALSE;

      $bundle_fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $node_bundle->id());
      if (!isset($bundle_fields['field_paragraphs'])) {
        continue;
      }

      if ($config->get('paragraph_templates.paragraph_template_' . $node_bundle->id())) {
        $default_entity = \Drupal::entityTypeManager()->getStorage('node')
          ->load($config->get('paragraph_templates.paragraph_template_' . $node_bundle->id())[0]['target_id']);
      }

      $form['paragraph_templates']['paragraph_template_' . $node_bundle->id()] = [
        '#type' => 'entity_autocomplete',
        '#title' => $node_bundle->label(),
        '#target_type' => 'node',
        '#selection_settings' => [
          'target_bundles' => [$node_bundle->id()],
        ],
        '#tags' => TRUE,
        '#size' => 30,
        '#maxlength' => 1024,
        '#default_value' => $default_entity ?? NULL,
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();

    $config = $this->configFactory->getEditable('vb_paragraphs.settings');
    $config->set('default_unpublished', $values['default_unpublished']);

    foreach ($values as $key => $value) {
      if (substr($key, 0, 19) === 'paragraph_template_') {
        $config->set('paragraph_templates.' . $key, $value);
      }
    }

    $config->save();

    parent::submitForm($form, $form_state);
  }

}

<?php

namespace Drupal\vb_paragraphs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\paragraphs_library\Entity\LibraryItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class VbParagraphsCloneFromTemplate.
 *
 * @package Drupal\vb_paragraphs\Form
 */
class VbParagraphsCloneFromTemplate extends FormBase {

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Provides an interface for form building and processing.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;


  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $class = parent::create($container);
    $class->entityTypeManager = $container->get('entity_type.manager');
    $class->formBuilder = $container->get('form_builder');
    return $class;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_paragraphs_clone_from_template';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL, $field = NULL) {
    // Set the paragraph to the form state.
    $form_state->addBuildInfo('node', $node);
    $form_state->addBuildInfo('field', $field);

    $template_node = $this->getTemplateByNodeType($node->getType());

    if (!isset($template_node)) {
      return [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No template found.'),
      ];
    }

    $form['message'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Paragraphs will be cloned from template:') . ' ' . $template_node->label() ,
    ];

    $form['clone_template'] = [
      '#type' => 'button',
      '#name' => 'clone_template',
      '#value' => $this->t('Clone'),
      '#ajax' => [
        'callback' => [get_class($this), 'cloneFromTemplate'],
        'effect' => 'fade',
      ],
    ];

    $form['cancel'] = [
      '#type' => 'button',
      '#name' => 'cancel',
      '#value' => $this->t('Cancel'),
      '#ajax' => [
        'callback' => [get_class($this), 'closeModal'],
        'effect' => 'fade',
      ],
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo create an ajax fallback
  }

  /**
   * {@inheritdoc}
   */
  public function cloneFromTemplate(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $node = $build_info['node'];
    $field = $build_info['field'];

    $langcode = \Drupal::service('language_manager')->getCurrentLanguage()->getId();

    $config = \Drupal::config('vb_paragraphs.settings');

    if ($config->get('paragraph_templates.paragraph_template_' . $node->getType())) {
      $template_node =  \Drupal::entityTypeManager()->getStorage('node')
        ->load($config->get('paragraph_templates.paragraph_template_' . $node->getType())[0]['target_id']);

      // If template has translation, we load the translated one.
      if ($template_node->hasTranslation($langcode)) {
        $template_node = $template_node->getTranslation($langcode);
      }
    }

    if (isset($template_node) && $template_node->hasField('field_paragraphs') && !$template_node->get('field_paragraphs')->isEmpty()) {
      $values = $template_node->get('field_paragraphs')->getValue();
      $cloned_paragraphs = [];

      foreach ($values as $value) {
        $source_paragraph = \Drupal::entityTypeManager()->getStorage('paragraph')
          ->load($value['target_id']);

        // Make a copy of the paragraph. Use the Replicate module, if it is enabled.
        if (\Drupal::hasService('replicate.replicator')) {
          $duplicate_paragraph = \Drupal::getContainer()->get('replicate.replicator')->replicateEntity($source_paragraph);
        }
        else {
          $duplicate_paragraph = $source_paragraph->createDuplicate();
        }

        $duplicate_paragraph->save();
        $cloned_paragraphs[] = [
          'target_id' => $duplicate_paragraph->id(),
          'target_revision_id' => $duplicate_paragraph->getRevisionId(),
        ];
      }

      // Attach the cloned paragraphs to the node.
      $node->field_paragraphs->setValue($cloned_paragraphs);
      $node->save();
    }

    $response = new AjaxResponse();

    // Refresh the paragraphs field.
    $response->addCommand(
      new ReplaceCommand(
        '#paragraphs-empty',
        $node->get($field)->view('default')
      )
    );

    // Close dialog.
    $response->addCommand(new CloseDialogCommand('.modal'));
    $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
    $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
    $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function closeModal(array $form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand('.modal'));

    return $response;
  }

  /**
   * Returns the template by Node type.
   *
   * @param $node_type
   *
   * @return \Drupal\Core\Entity\EntityInterface|false|null
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getTemplateByNodeType($node_type) {
    $config = \Drupal::config('vb_paragraphs.settings');

    if ($config->get('paragraph_templates.paragraph_template_' . $node_type)) {
      return \Drupal::entityTypeManager()->getStorage('node')
        ->load($config->get('paragraph_templates.paragraph_template_' . $node_type)[0]['target_id']);
    }

    return FALSE;
  }

}

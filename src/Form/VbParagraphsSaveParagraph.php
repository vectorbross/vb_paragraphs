<?php

namespace Drupal\vb_paragraphs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Class CleanupUrlAliases.
 *
 * @package Drupal\vb_paragraphs\Form
 */
class VbParagraphsSaveParagraph extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_paragraphs_save_paragraph';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $paragraph = NULL) {

    // Set the paragraph to the form state.
    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#size' => 60,
      '#maxlength' => 128,
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => [get_class($this), 'saveParagraphAjax'],
        'effect' => 'fade',
      ],
    ];
    $form_state->addBuildInfo('paragraph', $paragraph);

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo create an ajax fallback
  }

  /**
   * {@inheritdoc}
   */
  public function saveParagraphAjax(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $paragraph = $build_info['paragraph'];


   if (!$paragraph->getParagraphType()->getThirdPartySetting('paragraphs_library', 'allow_library_conversion', FALSE)) {
      throw new \Exception(sprintf('%s cannot be converted to library item per configuration', $paragraph->bundle()));
    }

    // Ensure that we work with the default language as the active one.
    $paragraph = $paragraph->getUntranslated();

    // Make a copy of the paragraph. Use the Replicate module, if it is enabled.
    if (\Drupal::hasService('replicate.replicator')) {
      $duplicate_paragraph = \Drupal::getContainer()->get('replicate.replicator')->replicateEntity($paragraph);
    }
    else {
      $duplicate_paragraph = $paragraph->createDuplicate();
    }
    $duplicate_paragraph->save();

    $library_item = LibraryItem::create([
      'paragraphs' => $duplicate_paragraph,
      'langcode' => $paragraph->language()->getId(),
      'label' => $form_state->getValue('title')
    ]);

    $library_item->save();

    $response = new AjaxResponse();
    $response->addCommand(new CloseDialogCommand());
    return $response;
  }


}

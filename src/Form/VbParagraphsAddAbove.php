<?php

namespace Drupal\vb_paragraphs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Class CleanupUrlAliases.
 *
 * @package Drupal\vb_paragraphs\Form
 */
class VbParagraphsAddAbove extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_paragraphs_add_above';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $paragraph = NULL) {

    // Set the paragraph to the form state.
    $form_state->addBuildInfo('paragraph', $paragraph);

    // Render the sets.
    $sets = \Drupal::entityTypeManager()->getListBuilder('paragraphs_library_item')->load();


    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('paragraphs_library_item');
    if (empty($sets)) {
      $form['empty_text'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No reusable parapgraphs found.'),
      ];
    }
    foreach ($sets as $set) {
      $form['sets'][$set->id()] = [
        '#type' => 'container',
      ];
      $form['sets'][$set->id()]['title'] = ['#markup' => '<h3>' . $set->label() . '</h3>'];
      $form['sets'][$set->id()]['element'] = $set->paragraphs->view([
        'type' => 'entity_reference_revisions_entity_view',
        'label' => 'hidden',
        'settings' => array(
          'view_mode' => 'preview'
        ),
      ]);
      $form['sets'][$set->id()]['add_set'] = [
        '#type' => 'button',
        '#name' => $set->id() . '_add_set',
        '#value' => $this->t('Add'),
        '#ajax' => [
          'callback' => [$this, 'addMoreAjax'],
          'effect' => 'fade',
        ],
      ];
    }

    $user = \Drupal::currentUser();

    // $form['add_set'] = [
    //   '#type' => 'link',
    //   '#title' => $this->t('Add a new item'),
    //   '#url' => Url::fromRoute('entity.paragraphs_library_item.add_form', [
    //     'destination' => \Drupal::destination()->get(),
    //   ]),
    // ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo create an ajax fallback
  }

  /**
   * {@inheritdoc}
   */
  public function addMoreAjax(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $triggering_paragraph = $build_info['paragraph'];
    $trigger = $form_state->getTriggeringElement()['#name'];
    $response = new AjaxResponse();

    $set = substr($trigger, 0, -8);
    if (is_numeric($set)) {

      $set = LibraryItem::load($set);
      $target_id = $set->get('paragraphs')->getValue()[0]['target_id'];
      $set_paragraph = Paragraph::load($target_id);

      $parent = $triggering_paragraph->getParentEntity();

      $parent_type = $parent->getEntityTypeId();
      $parent_bundle = $parent->getType();
      $parent_entity_id = $parent->id();
      $parent_field_name = $triggering_paragraph->get('parent_field_name')->getValue()[0]['value'];

      $paragraph_items = $parent->$parent_field_name->getValue();
      $paragraphs_new = [];

      foreach ($paragraph_items as $delta => $paragraph_item) {
        if ($paragraph_item['target_id'] == $triggering_paragraph->id()) {

          $new_paragraph = $set_paragraph->createDuplicate();
          // Set the language of the parent
          $langcode = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
          $new_paragraph->set('langcode', $langcode);

          // New paragraphs are unpublished by default?
          $config = \Drupal::config('vb_paragraphs.settings');
          if($config->get('default_unpublished')) {
            $new_paragraph->setUnpublished();
          }

          // For synchronous paragraph-translations, we create the
          // translations by default and unpublish them.
          if (!$parent->get($parent_field_name)->getFieldDefinition()->isTranslatable()) {
            $languages = \Drupal::languageManager()->getLanguages();
            $paragraph_array = $new_paragraph->toArray();
            $paragraph_array['status'][0]['value'] = FALSE;

            foreach ($languages as $lang => $language) {
              if ($lang != $langcode) {
                $new_paragraph->addTranslation($lang, $paragraph_array);
              }
            }
          }

          $new_paragraph->save();
          $paragraphs_new[] = [
            'target_id' => $new_paragraph->id(),
            'target_revision_id' => $new_paragraph->getRevisionId(),
          ];
        }
        $paragraphs_new[] = $paragraph_item;
      }
      $parent->$parent_field_name->setValue($paragraphs_new);
      $parent->save();

      $identifier = '[data-paragraphs-frontend-ui=' . $parent_field_name . '-' . $parent->id() . ']';
      // Refresh the paragraphs field.
      $response->addCommand(
        new ReplaceCommand(
          $identifier,
          $parent->get($parent_field_name)->view('default')
        )
      );
      $response->addCommand(new CloseDialogCommand('.modal'));
      $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
      $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
      $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));
    }
    return $response;
  }


}

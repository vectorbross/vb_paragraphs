<?php

namespace Drupal\vb_paragraphs\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\CloseDialogCommand;
use Drupal\paragraphs_library\Entity\LibraryItem;

/**
 * Class CleanupUrlAliases.
 *
 * @package Drupal\vb_paragraphs\Form
 */
class VbParagraphsAddInitial extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'vb_paragraphs_add_initial';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $node = NULL, $field = NULL) {

    // Set the paragraph to the form state.
    $form_state->addBuildInfo('node', $node);
    $form_state->addBuildInfo('field', $field);

    // Render the sets.
    $sets = \Drupal::entityTypeManager()->getListBuilder('paragraphs_library_item')->load();

    $view_builder = \Drupal::entityTypeManager()->getViewBuilder('paragraphs_library_item');
    if (empty($sets)) {
      $form['empty_text'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('No reusable parapgraphs found.'),
      ];
    }

    $categories_all = array();

    foreach ($sets as $set) {
      $categories = array();

      // Field_category will be removed, so later this code can be removed?
      // Created a check to see if field still exists.
      if ($set->hasField('field_category') && !$set->get('field_category')->isEmpty()) {
        foreach($set->field_category->getValue() as $category) {
          $term = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->load($category['target_id']);
          $categories[] = 'category-' . $category['target_id'];
          $categories_all[$category['target_id']] = $term->label();
        }
      }

      $categories[] = 'paragraph-wrapper';

      $form['sets'][$set->id()] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => $categories,
        ]
      ];

      //$form['sets'][$set->id()]['element'] = $view_builder->view($set);
      $form['sets'][$set->id()]['title'] = ['#markup' => '<h3>' . $set->label() . '</h3>'];
      $form['sets'][$set->id()]['element'] = $set->paragraphs->view([
        'type' => 'entity_reference_revisions_entity_view',
        'label' => 'hidden',
        'settings' => array(
          'view_mode' => 'preview'
        ),
      ]);
      $form['sets'][$set->id()]['add_set'] = [
        '#type' => 'button',
        '#name' => $set->id() . '_add_set',
        '#value' => $this->t('Add'),
        '#ajax' => [
          'callback' => [get_class($this), 'addInitialAjax'],
          'effect' => 'fade',
        ],
      ];
    }

    asort($categories_all);

    // $form['categories'] = [
    //   '#type' => 'container',
    //   '#weight' => -1
    // ];
    // foreach($categories_all as $id => $category) {
    //   $form['categories'][$category] = [
    //     '#type' => 'button',
    //     '#value' => $category,
    //     '#attributes' => [
    //       'class' => ['btn', 'btn-default', 'js-paragraph-category', 'btn--category-' . $id],
    //       'data-toggle' => 'category-' . $id
    //     ],
    //   ];
    // }

    $user = \Drupal::currentUser();

    // $form['add_set'] = [
    //   '#type' => 'link',
    //   '#title' => $this->t('Add a new item'),
    //   '#url' => Url::fromRoute('entity.paragraphs_library_item.add_form', [
    //     'destination' => \Drupal::destination()->get(),
    //   ]),
    // ];

    $form['#attached']['library'][] =  'vb_paragraphs/admin';

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // @todo create an ajax fallback
  }

  /**
   * {@inheritdoc}
   */
  public function addInitialAjax(array $form, FormStateInterface $form_state) {
    $build_info = $form_state->getBuildInfo();
    $node = $build_info['node'];
    $field = $build_info['field'];
    $response = new AjaxResponse();

    $trigger = $form_state->getTriggeringElement()['#name'];
    $set = substr($trigger, 0, -8);
    if (is_numeric($set)) {

      // Load the library item paragraph
      $set = LibraryItem::load($set);
      $target_id = $set->get('paragraphs')->getValue()[0]['target_id'];
      $set_paragraph = Paragraph::load($target_id);

      // Create a duplicate of the library paragraph
      $paragraph = $set_paragraph->createDuplicate();
      $langcode = \Drupal::service('language_manager')->getCurrentLanguage()->getId();
      $paragraph->set('langcode', $langcode);

      // New paragraphs are unpublished by default?
      $config = \Drupal::config('vb_paragraphs.settings');
      if($config->get('default_unpublished')) {
        $paragraph->setUnpublished();
      }

      // For synchronous paragraph-translations, we create the
      // translations by default and unpublish them.
      if (!$node->get($field)->getFieldDefinition()->isTranslatable()) {
        $languages = \Drupal::languageManager()->getLanguages();
        $paragraph_array = $paragraph->toArray();
        $paragraph_array['status'][0]['value'] = FALSE;

        foreach ($languages as $lang => $language) {
          if ($lang != $langcode) {
            $paragraph->addTranslation($lang, $paragraph_array);
          }
        }
      }

      $paragraph->save();


      // Inject the newly created paragraph into the nodes paragraph field
      $field_value = [
        'target_id' => $paragraph->id(),
        'target_revision_id' => $paragraph->getRevisionId(),
      ];
      $node->$field->setValue($field_value);
      $node->save();

      // Refresh the paragraphs field.
      $response->addCommand(
        new ReplaceCommand(
          '[data-paragraphs-frontend-ui=field_paragraphs-' . $node->id() . ']',
          $node->get($field)->view('default')
        )
      );
      $response->addCommand(new CloseDialogCommand('.modal'));
      $response->addCommand(new InvokeCommand('body', 'removeClass', ['modal-open']));
      $response->addCommand(new InvokeCommand('body', 'css', ['padding-right', '0']));
      $response->addCommand(new InvokeCommand('body', 'css', ['padding-top', '0']));

    }
    return $response;
  }

}

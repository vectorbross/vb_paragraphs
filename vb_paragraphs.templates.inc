<?php

use Drupal\paragraphs\Entity\Paragraph;
use Drupal\paragraphs_library\Entity\LibraryItem;
use Drupal\taxonomy\Entity\Term;
use Drupal\media\Entity\Media;

/**
 * @see vb_paragraphs.install
 */
class ParagraphThemplates {
	/**
	* The placeholder images
	*/
	protected $images;

	/**
	* The category terms
	*/
	protected $categories;

	/**
	* Some placeholder text
	*/
	protected $longtext;

	/**
	* Some shorter placeholder text
	*/
	protected $shorttext;


	public function __construct() {
		$this->categories = [
			'Text' => NULL,
			'Image' => NULL,
			'Video' => NULL,
			'Gallery' => NULL,
			'USP' => NULL,
			'Call to action' => NULL,
			'Cards' => NULL,
			'Counters' => NULL,
			'Tabs' => NULL,
		];
		$this->images = [
			'placeholder1' => 'Placeholder image 1',
			'placeholder2' => 'Placeholder image 2',
			'placeholder3' => 'Placeholder image 3',
			'placeholder4' => 'Placeholder image 4',
			'placeholder5' => 'Placeholder image 5',
			'placeholder6' => 'Placeholder image 6',
		];
		$this->logos = [
			'logo1' => 'Placeholder logo 1',
			'logo2' => 'Placeholder logo 2',
			'logo3' => 'Placeholder logo 3',
			'logo4' => 'Placeholder logo 4',
			'logo5' => 'Placeholder logo 5',
		];
		$this->persons = [
			'person1' => 'John Doe',
			'person2' => 'Jane Doe',
		];
		$this->files = [
			'file1' => 'Placeholder file 1',
			'file2' => 'Placeholder file 2',
		];
		$this->longtext = "<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p><p>Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo.</p><p>Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus.</p><p>Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.</p><p>Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,</p>";
		$this->shorttext = "<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.</p>";
	}

	 /**
	 * Create the categories we use to bundle the templates.
	 */
	private function createCategoryTerms() {
		foreach($this->categories as $name => $tid) {
			$term = Term::create([
			  'name' => $name, 
			  'vid' => 'paragraph_categories',
			]);
			$term->save();
			$this->categories[$name] = $term->id();
		}
	}

	/**
	 * Create some placeholder images to be used in the templates.
	 */
	private function createPlaceholderImages() {
		foreach($this->images as $name => $title) {
			$file_data = file_get_contents(__DIR__ . '/images/placeholders/' . $name . '.jpg');
			$file = file_save_data($file_data, 'public://' . $name . '.jpg', FILE_EXISTS_REPLACE);
			$media = Media::create([
	          'name' => $title,
	          'bundle' => 'image',
	          'uid' => 1,
	          'status' => 1,
	          'field_media_image' => [
	            'target_id' => $file->id(),
	            'alt' => $title,
	          ],
	        ]);
			$media->save();
			$this->images[$name] = $media->id();
		}
	}

	/**
	 * Create some placeholder persons to be used in the templates.
	 */
	private function createPlaceholderPersons() {
		foreach($this->persons as $name => $title) {
			$file_data = file_get_contents(__DIR__ . '/images/placeholders/' . $name . '.jpg');
			$file = file_save_data($file_data, 'public://' . $name . '.jpg', FILE_EXISTS_REPLACE);
			$media = Media::create([
	          'name' => $title,
	          'bundle' => 'persoon',
	          'uid' => 1,
	          'status' => 1,
	          'field_media_image' => [
	            'target_id' => $file->id(),
	            'alt' => $title,
	          ],
	          'field_function' => 'Lorem ipsum',
	        ]);
			$media->save();
			$this->images[$name] = $media->id();
		}
	}

	/**
	 * Create some placeholder logos to be used in the templates.
	 */
	private function createPlaceholderLogos() {
		foreach($this->logos as $name => $title) {
			$file_data = file_get_contents(__DIR__ . '/images/placeholders/' . $name . '.png');
			$file = file_save_data($file_data, 'public://' . $name . '.jpg', FILE_EXISTS_REPLACE);
			$media = Media::create([
	          'name' => $title,
	          'bundle' => 'brand',
	          'uid' => 1,
	          'status' => 1,
	          'field_media_image' => [
	            'target_id' => $file->id(),
	            'alt' => $title,
	          ]
	        ]);
			$media->save();
			$this->logos[$name] = $media->id();
		}
	}

	/**
	 * Create some placeholder files to be used in the templates.
	 */
	private function createPlaceholderFiles() {
		foreach($this->files as $name => $title) {
			$file_data = file_get_contents(__DIR__ . '/images/placeholders/' . $name . '.pdf');
			$file = file_save_data($file_data, 'public://' . $name . '.pdf', FILE_EXISTS_REPLACE);
			$media = Media::create([
	          'name' => $title,
	          'bundle' => 'document',
	          'uid' => 1,
	          'status' => 1,
	          'field_media_document' => [
	            'target_id' => $file->id(),
	            'display' => 1,
	            'description' => $title,
	          ]
	        ]);
			$media->save();
			$this->files[$name] = $media->id();
		}
	}


	/**
	 * Initialize all templates
	 */
	public function createTemplates() {
		// We start by creating terms and images to be used in the templates
		//$this->createCategoryTerms();
		$this->createPlaceholderImages();
		//$this->createPlaceholderPersons();
		$this->createPlaceholderLogos();
		$this->createPlaceholderFiles();

		// TODO: change the media image and document form displays

		// Create the actual templates
		$this->createAccordionTemplates();
		$this->createBrandsTemplates();
		$this->createCalloutTemplates();
		$this->createCallToActionTemplates();
		$this->createCardsTemplates();
		$this->createCountersTemplates();
		$this->createDownloadsTemplates();
		$this->createFormTemplates();
		$this->createFormTextTemplates();
		$this->createGalleryTemplates();
		$this->createImageTemplates();
		$this->createImageTextTemplates();
		$this->createMapTemplates();
		//$this->createPersonsTemplates();
		$this->createQuotesTemplates();
		$this->createSliderTextTemplates();
		$this->createTabsTemplates();
		$this->createTextTemplates();
		$this->createUspTextTemplates();
		$this->createVideoTemplates();
		$this->createVideoTextTemplates();
	}

	/**
	 * Create the Accordion templates.
	 */
	public function createAccordionTemplates() {
		$accordion1 = Paragraph::create([
			'type' => 'accordion_item',
			'field_title' => 'Eerste accordion item',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$accordion1->save();

		$accordion2 = Paragraph::create([
			'type' => 'accordion_item',
			'field_title' => 'Tweede accordion item',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$accordion2->save();

		$accordion3 = Paragraph::create([
			'type' => 'accordion_item',
			'field_title' => 'Derde accordion item',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$accordion3->save();

		$paragraph = Paragraph::create([
			'type' => 'accordion',
			'field_title' => 'Dit is een accordion',
			'field_accordion_items' => [
				[
					'target_id' => $accordion1->id(),
					'target_revision_id' => $accordion1->getRevisionId()
				],
				[
					'target_id' => $accordion2->id(),
					'target_revision_id' => $accordion2->getRevisionId()
				],
				[
					'target_id' => $accordion3->id(),
					'target_revision_id' => $accordion3->getRevisionId()
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Accordion',
		]);
		$library_item->save();
	}

	/**
	 * Create the Brands templates.
	 */
	public function createBrandsTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'brands',
			'field_title' => 'Dit zijn logo\'s',
			'field_brands' => [
				[
					'target_id' => $this->logos['logo1']
				],
				[
					'target_id' => $this->logos['logo2']
				],
				[
					'target_id' => $this->logos['logo3']
				],
				[
					'target_id' => $this->logos['logo4']
				],
				[
					'target_id' => $this->logos['logo5']
				],
				'field_brands_style' => 'grid'
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Logo\'s',
		]);
		$library_item->save();
	}

	/**
	 * Create the Callout templates.
	 */
	function createCalloutTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'callout',
			'field_title' => 'Dit is een callout',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Callout',
		]);
		$library_item->save();
	}

	/**
	 * Create the Call to action templates.
	 */
	function createCallToActionTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'call_to_action',
			'field_title' => 'Dit is een call to action',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
			'field_background' => 'primary',
			'field_links' => [
				[
					'uri' => 'internal:/',
					'title' => 'Button label'
				]
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Call to action',
		]);
		$library_item->save();
	}

	/**
	 * Create the Cards templates.
	 */
	public function createCardsTemplates() {
		$card1 = Paragraph::create([
			'type' => 'card',
			'field_title' => 'Dit is een card',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
			'field_media_image' => [
				'target_id' => $this->images['placeholder1']
			]
		]);
		$card1->save();

		$card2 = Paragraph::create([
			'type' => 'card',
			'field_title' => 'Dit is een card met een link',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
			'field_link' => [
				'uri' => 'internal:/'
			],
			'field_media_image' => [
				'target_id' => $this->images['placeholder2']
			]
		]);
		$card2->save();

		$card3 = Paragraph::create([
			'type' => 'card',
			'field_title' => 'Dit is nog een card met een langere titel',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
			'field_media_image' => [
				'target_id' => $this->images['placeholder3']
			]
		]);
		$card3->save();

		$card4 = Paragraph::create([
			'type' => 'card',
			'field_title' => 'Dit is een card',
			'field_text' => [
				'value'  => $this->shorttext,
				'format' => 'basic_html'
			],
			'field_media_image' => [
				'target_id' => $this->images['placeholder1']
			]
		]);
		$card4->save();

		$paragraph = Paragraph::create([
			'type' => 'cards',
			'field_title' => 'Dit zijn cards',
			'field_cards' => [
				[
					'target_id' => $card1->id(),
					'target_revision_id' => $card1->getRevisionId()
				],
				[
					'target_id' => $card2->id(),
					'target_revision_id' => $card2->getRevisionId()
				],
				[
					'target_id' => $card3->id(),
					'target_revision_id' => $card3->getRevisionId()
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Cards',
		]);
		$library_item->save();
	}

	/**
	 * Create the Counters templates.
	 */
	public function createCountersTemplates() {
		$counter1 = Paragraph::create([
			'type' => 'counter',
			'field_text' => [
				'value'  => '<h3>Lorem ipsum dolor sit amet</h3>',
				'format' => 'basic_html'
			],
			'field_count_from' => '0',
			'field_count_to' => '50',
			'field_icon' => 'fas fa-certificate'
		]);
		$counter1->save();

		$counter2 = Paragraph::create([
			'type' => 'counter',
			'field_text' => [
				'value'  => '<h3>Lorem ipsum dolor sit amet</h3>',
				'format' => 'basic_html'
			],
			'field_count_from' => '0',
			'field_count_to' => '99',
			'field_suffix' => '%',
			'field_icon' => 'fas fa-thumbs-up'
		]);
		$counter2->save();

		$counter3 = Paragraph::create([
			'type' => 'counter',
			'field_text' => [
				'value'  => '<h3>Lorem ipsum dolor sit amet</h3>',
				'format' => 'basic_html'
			],
			'field_count_from' => '0',
			'field_count_to' => '10',
			'field_icon' => 'fas fa-smile'
		]);
		$counter3->save();

		$counter4 = Paragraph::create([
			'type' => 'counter',
			'field_text' => [
				'value'  => '<h3>Lorem ipsum dolor sit amet</h3>',
				'format' => 'basic_html'
			],
			'field_count_from' => '0',
			'field_count_to' => '75',
			'field_icon' => 'fas fa-comment'
		]);
		$counter4->save();

		$paragraph = Paragraph::create([
			'type' => 'counters',
			'field_title' => 'Dit zijn counters',
			'field_counters' => [
				[
					'target_id' => $counter1->id(),
					'target_revision_id' => $counter1->getRevisionId()
				],
				[
					'target_id' => $counter2->id(),
					'target_revision_id' => $counter2->getRevisionId()
				],
				[
					'target_id' => $counter3->id(),
					'target_revision_id' => $counter3->getRevisionId()
				],
				[
					'target_id' => $counter4->id(),
					'target_revision_id' => $counter4->getRevisionId()
				],
			],
			'field_columns' => 'four'
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Counters',
		]);
		$library_item->save();
	}

	/**
	 * Create the Downloads templates.
	 */
	public function createDownloadsTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'downloads',
			'field_title' => 'Dit zijn downloads',
			'field_media_files' => [
				[
					'target_id' => $this->files['file1']
				],
				[
					'target_id' => $this->files['file2']
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Downloads',
		]);
		$library_item->save();
	}

	/**
	 * Create the Form templates.
	 */
	public function createFormTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'form',
			'field_title' => 'Dit is een formulier',
			'field_form' => [
				'target_id' => 'contact',
				'status' => 'open'
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Form',
		]);
		$library_item->save();
	}

	/**
	 * Create the Form & text templates.
	 */
	public function createFormTextTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'form_text',
			'field_form' => [
				'target_id' => 'contact',
				'status' => 'open'
			],
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Form & text',
		]);
		$library_item->save();
	}

	/**
	 * Create the Gallery templates.
	 */
	public function createGalleryTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'gallery',
			'field_title' => 'Dit is een gallery',
			'field_media_images' => [
				[
					'target_id' => $this->images['placeholder1']
				],
				[
					'target_id' => $this->images['placeholder2']
				],
				[
					'target_id' => $this->images['placeholder3']
				],
				[
					'target_id' => $this->images['placeholder4']
				],
				[
					'target_id' => $this->images['placeholder5']
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Gallery',
		]);
		$library_item->save();
	}

	/**
	 * Create the Image templates.
	 */
	public function createImageTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'image',
			'field_title' => 'Dit is een image',
			'field_caption' => 'Dit is een optionele caption',
			'field_media_image' => [
				[
					'target_id' => $this->images['placeholder5']
				]
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Image',
		]);
		$library_item->save();
	}

	/**
	 * Create the Image & text templates.
	 */
	public function createImageTextTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'image_text',
			'field_media_images' => [
				[
					'target_id' => $this->images['placeholder3']
				]
			],
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Image & text',
		]);
		$library_item->save();
	}

	/**
	 * Create the Map templates.
	 */
	public function createMapTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'map',
			'field_address' => [
				'country_coode' => 'BE',
				'locality' => 'Leuven',
				'postal_code' => '3000',
				'address_line1' => 'Sluisstraat 79',
				'organization' => 'Vector BROSS',
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Map',
		]);
		$library_item->save();
	}

	/**
	 * Create the Persons templates.
	 */
	public function createPersonsTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'persons',
			'field_title' => 'Dit zijn personen',
			'field_persons' => [
				[
					'target_id' => $this->persons['person1']
				],
				[
					'target_id' => $this->persons['person2']
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Persons',
		]);
		$library_item->save();
	}

	/**
	 * Create the Quotes templates.
	 */
	function createQuotesTemplates() {
		$quote1 = Paragraph::create([
			'type' => 'quote',
			'field_author' => 'John Doe',
			'field_occupation' => 'Developer, Vector BROSS',
			'field_quote' => 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.'
		]);
		$quote1->save();

		$quote2 = Paragraph::create([
			'type' => 'quote',
			'field_author' => 'Jane Doe',
			'field_occupation' => 'Designer, Vector BROSS',
			'field_quote' => 'Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus.'
		]);
		$quote2->save();

		$paragraph = Paragraph::create([
			'type' => 'quotes',
			'field_title' => 'Dit zijn quotes',
			'field_quotes_style' => 'slider',
			'field_quotes' => [
				[
					'target_id' => $quote1->id(),
					'target_revision_id' => $quote1->getRevisionId()
				],
				[
					'target_id' => $quote2->id(),
					'target_revision_id' => $quote2->getRevisionId()
				],
			]
		]);
		$paragraph->save();

		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Quotes'
		]);
		$library_item->save();
	}

	/**
	 * Create the Slider & text templates.
	 */
	function createSliderTextTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'image_text',
			'field_media_images' => [
				[
					'target_id' => $this->images['placeholder1']
				],
				[
					'target_id' => $this->images['placeholder3']
				],
				[
					'target_id' => $this->images['placeholder6']
				],
			],
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Slider & text',
		]);
		$library_item->save();
	}

	/**
	 * Create the Tabs templates.
	 */
	public function createTabsTemplates() {
		$tab1 = Paragraph::create([
			'type' => 'tab',
			'field_title' => 'Eerste tab',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$tab1->save();

		$tab2 = Paragraph::create([
			'type' => 'tab',
			'field_title' => 'Tweede tab',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$tab2->save();

		$tab3 = Paragraph::create([
			'type' => 'tab',
			'field_title' => 'Derde tab',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			]
		]);
		$tab3->save();

		$paragraph = Paragraph::create([
			'type' => 'tabs',
			'field_title' => 'Dit zijn tabs',
			'field_tabs' => [
				[
					'target_id' => $tab1->id(),
					'target_revision_id' => $tab1->getRevisionId()
				],
				[
					'target_id' => $tab2->id(),
					'target_revision_id' => $tab2->getRevisionId()
				],
				[
					'target_id' => $tab3->id(),
					'target_revision_id' => $tab3->getRevisionId()
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Tabs',
		]);
		$library_item->save();
	}

	/**
	 * Create the Text templates.
	 */
	public function createTextTemplates() {
		$text1 = Paragraph::create([
			'type' => 'text',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$text1->save();	

		$text2 = Paragraph::create([
			'type' => 'text',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$text2->save();

		$text3 = Paragraph::create([
			'type' => 'text',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$text3->save();

		$paragraph = Paragraph::create([
			'type' => 'text_columns',
			'field_text_columns' => [
				[
					'target_id' => $text1->id(),
					'target_revision_id' => $text1->getRevisionId()
				],
			],
			'field_columns' => 'one'
		]);
		$paragraph->save();

		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Text',
		]);
		$library_item->save();

		$paragraph = Paragraph::create([
			'type' => 'text_columns',
			'field_text_columns' => [
				[
					'target_id' => $text2->id(),
					'target_revision_id' => $text2->getRevisionId()
				],
				[
					'target_id' => $text3->id(),
					'target_revision_id' => $text3->getRevisionId()
				],
			],
			'field_columns' => 'two'
		]);
		$paragraph->save();

		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Text 2 columns',
		]);
		$library_item->save();
	}

	/**
	 * Create the Usp & text templates.
	 */
	public function createUspTextTemplates() {
		$usp1 = Paragraph::create([
			'type' => 'usp',
			'field_title' => 'Lorem ipsum dolor sit amet',
			'field_icon' => 'fa-check-square'
		]);
		$usp1->save();

		$usp2 = Paragraph::create([
			'type' => 'usp',
			'field_title' => 'Lorem ipsum dolor sit amet',
			'field_icon' => 'fa-check-square'
		]);
		$usp2->save();

		$usp3 = Paragraph::create([
			'type' => 'usp',
			'field_title' => 'Lorem ipsum dolor sit amet',
			'field_icon' => 'fa-check-square'
		]);
		$usp3->save();

		$paragraph = Paragraph::create([
			'type' => 'usp_text',
			'field_title' => 'Dit zijn USPs en tekst',
			'field_text' => [
				'value' => $this->longtext,
				'format' => 'basic_html'
			],
			'field_usps' => [
				[
					'target_id' => $usp1->id(),
					'target_revision_id' => $usp1->getRevisionId()
				],
				[
					'target_id' => $usp2->id(),
					'target_revision_id' => $usp2->getRevisionId()
				],
				[
					'target_id' => $usp3->id(),
					'target_revision_id' => $usp3->getRevisionId()
				],
			]
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'USP & text',
		]);
		$library_item->save();
	}

	/**
	 * Create the Video templates.
	 */
	public function createVideoTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'video',
			'field_title' => 'Dit is een video',
			'field_caption' => 'Dit is een optionele caption',
			'field_video' => 'https://www.youtube.com/watch?v=uh4dTLJ9q9o'
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Video',
		]);
		$library_item->save();
	}

	/**
	 * Create the Video & text templates.
	 */
	public function createVideoTextTemplates() {
		$paragraph = Paragraph::create([
			'type' => 'video_text',
			'field_video' => 'https://www.youtube.com/watch?v=uh4dTLJ9q9o',
			'field_text' => [
				'value'  => $this->longtext,
				'format' => 'basic_html'
			],
		]);
		$paragraph->save();	
		$library_item = LibraryItem::create([
			'paragraphs' => $paragraph,
			'langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
			'label' => 'Video & text',
		]);
		$library_item->save();
	}
}